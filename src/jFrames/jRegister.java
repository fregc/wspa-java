package jFrames;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JButton;

public class jRegister {

	private JFrame frame;
	private JTextField textField;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					jRegister window = new jRegister();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public jRegister() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(160, 60, 114, 19);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(160, 112, 114, 19);
		frame.getContentPane().add(passwordField);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(160, 164, 114, 19);
		frame.getContentPane().add(passwordField_1);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setBounds(12, 62, 128, 15);
		frame.getContentPane().add(lblLogin);
		
		JLabel lblPasssword = new JLabel("Passsword");
		lblPasssword.setBounds(12, 114, 128, 15);
		frame.getContentPane().add(lblPasssword);
		
		JLabel lblRepeatPassword = new JLabel("Repeat password");
		lblRepeatPassword.setBounds(12, 166, 130, 15);
		frame.getContentPane().add(lblRepeatPassword);
		
		JButton btnNewButton = new JButton("Register");
		btnNewButton.setBounds(160, 205, 117, 25);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnGoToLogin = new JButton("Go to login");
		btnGoToLogin.setBounds(321, 12, 117, 25);
		frame.getContentPane().add(btnGoToLogin);
	}

}
