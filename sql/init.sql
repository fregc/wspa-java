CREATE TABLE IF NOT EXISTS POSTS (
    id       SERIAL  PRIMARY KEY     NOT NULL,
    title    TEXT     NOT NULL,
	body     TEXT     NOT NULL,
    likes    INTEGER   DEFAULT 0,
    userId   INTEGER  NOT NULL,
    createdAt TIMESTAMP DEFAULT now(),
    updateddAt TIMESTAMP DEFAULT now());
    
    
CREATE TABLE IF NOT EXISTS COMMENTS (
    id      SERIAL  PRIMARY KEY     NOT NULL,
	body     TEXT    NOT NULL,
    orderInPost    INTEGER NOT NULL DEFAULT 0,
    postId   INTEGER  NOT NULL,
    userId   INTEGER  NOT NULL,
    createdAt TIMESTAMP DEFAULT now(),
    updateddAt TIMESTAMP DEFAULT now());
    
CREATE TABLE IF NOT EXISTS USERS (
    id       SERIAL  PRIMARY KEY     NOT NULL,
    username    TEXT    UNIQUE          NOT NULL,
    createdAt TIMESTAMP DEFAULT now(),
    updateddAt TIMESTAMP DEFAULT now()););